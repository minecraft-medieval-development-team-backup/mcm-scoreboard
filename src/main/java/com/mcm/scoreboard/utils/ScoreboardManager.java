package com.mcm.scoreboard.utils;

import com.mcm.core.cache.MiningTime;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.database.MinesDb;
import com.mcm.core.utils.TimeUtil;
import com.mcm.scoreboard.builders.MainScoreboard;
import com.mcm.scoreboard.builders.ScoreboardMines;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import java.text.DecimalFormat;
import java.util.Date;

public class ScoreboardManager {

    public static void createScoreboard(Player player) {
        new ScoreboardBuilder(player);
    }

    public static void reloadMainScoreboard(Player player) {
        MainScoreboard.build(player);
    }

    public static void loadScoreboardMines(Player player, int mine) {
        ScoreboardMines.build(player, mine);
    }

    public static void removeScoreboard(Player player) {
        player.setScoreboard(null);
    }

    public static void updateMina(Player player, String mina) {
        player.getScoreboard().getTeam("mina").setSuffix(ChatColor.AQUA + mina);
    }

    public static void updateAtual(Player player, int mina) {
        player.getScoreboard().getTeam("atual").setSuffix(ChatColor.GRAY + String.valueOf(MinesDb.getXP(player.getUniqueId().toString(), mina)) + ChatColor.GREEN + " (Lv" + MinesDb.getLevel(player.getUniqueId().toString(), mina) + ")");
    }

    public static void updateProximo(Player player, int mina) {
        player.getScoreboard().getTeam("proximo").setSuffix("FAZER");
    }

    public static void updateTempo(Player player, int mina) {
        long time = MinesDb.getTime(player.getUniqueId().toString(), mina);
        if (MiningTime.get(player.getUniqueId().toString()) != null) {
            player.getScoreboard().getTeam("tempo").setSuffix(ChatColor.GREEN + TimeUtil.getTime(MiningTime.get(player.getUniqueId().toString()).getTime()));
        } else if (((time - new Date().getTime()) / 1000) >= 1) {
            player.getScoreboard().getTeam("tempo").setSuffix(ChatColor.RED + "BLOQUEADO");
        } else {
            player.getScoreboard().getTeam("tempo").setSuffix(ChatColor.GREEN + "DESBLOQUEADO");
        }
    }

    public static void updateGroup(Player player) {
        String tag = com.mcm.core.database.TagDb.getTag(player.getUniqueId().toString());
        player.getScoreboard().getTeam("grupo").setSuffix(ScoreboardBuilder.prefix(tag));
        updateAllTab();
    }

    public static void updateCoins(Player player) {
        final DecimalFormat formatter = new DecimalFormat("#,###.00");
        if (com.mcm.core.database.CoinsDb.getCoins(player.getUniqueId().toString()) != 0) {
            player.getScoreboard().getTeam("moeda").setSuffix(ChatColor.YELLOW + "$" + formatter.format(com.mcm.core.database.CoinsDb.getCoins(player.getUniqueId().toString())));
        } else player.getScoreboard().getTeam("moeda").setSuffix("$0.0");
    }

    public static void updateGuilda(Player player) {
        String tag = com.mcm.core.database.TagDb.getTag(player.getUniqueId().toString());
        player.getScoreboard().getTeam("guilda").setSuffix(ChatColor.AQUA + GuildaDb.getGuildaP(player.getUniqueId().toString()));
    }

    public static void updateOnline(Player player) {
        player.getScoreboard().getTeam("online").setSuffix(ChatColor.GREEN + String.valueOf(Bukkit.getOnlinePlayers().size()));
    }

    public static void addOnTab(String name, String tag) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            Scoreboard scoreboard = player.getScoreboard();
            if (scoreboard.getTeam("000" + ScoreboardBuilder.sequency(tag)) == null) scoreboard.registerNewTeam("000" + ScoreboardBuilder.sequency(tag)).setPrefix(ScoreboardBuilder.tabPrefix(tag));
            scoreboard.getTeam("000" + ScoreboardBuilder.sequency(tag)).addEntry(name);
            scoreboard.getTeam("000" + ScoreboardBuilder.sequency(tag)).setColor(ScoreboardBuilder.colorType(tag));
        }
    }

    public static void updateAllTab() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            Scoreboard scoreboard = player.getScoreboard();

            for (Player target : Bukkit.getOnlinePlayers()) {
                String targetTag = com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString());
                if (scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)) == null) scoreboard.registerNewTeam("000" + ScoreboardBuilder.sequency(targetTag)).setPrefix(ScoreboardBuilder.tabPrefix(targetTag));
                scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)).addEntry(target.getName());
                scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)).setColor(ScoreboardBuilder.colorType(targetTag));
            }
            player.setScoreboard(scoreboard);
        }
    }
}
