package com.mcm.scoreboard.utils;

import com.mcm.core.Main;
import com.mcm.core.enums.GuildsServers;
import com.mcm.scoreboard.builders.MainScoreboard;
import com.mcm.scoreboard.builders.ScoreboardGuilda;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ScoreboardBuilder {

    public ScoreboardBuilder(Player player) {
        for (GuildsServers server : GuildsServers.values()) if (server.name().replaceAll("_", "-").equals(Main.server_name)) {
            ScoreboardGuilda.build(player);
            return;
        }
        MainScoreboard.build(player);
    }

    public static String tabPrefix(String tag) {
        if (tag.equalsIgnoreCase("superior")) return ChatColor.RED + "[STAFF] ";
        else if (tag.equalsIgnoreCase("gestor")) return ChatColor.RED + "[STAFF] ";
        else if (tag.equalsIgnoreCase("moderador")) return ChatColor.RED + "[STAFF] ";
        else if (tag.equalsIgnoreCase("suporte")) return ChatColor.RED + "[STAFF] ";
        else if (tag.equalsIgnoreCase("youtuber")) return ChatColor.RED + "[YouTuber] ";
        else if (tag.equalsIgnoreCase("vip")) return ChatColor.GOLD + "[VIP] ";
        else if (tag.equalsIgnoreCase("membro")) return ChatColor.GRAY + "";
        return null;
    }

    public static String prefix(String tag) {
        if (tag.equalsIgnoreCase("superior")) return ChatColor.DARK_RED + "[Superior]";
        else if (tag.equalsIgnoreCase("gestor")) return ChatColor.RED + "[Gestor]";
        else if (tag.equalsIgnoreCase("moderador")) return ChatColor.DARK_GREEN + "[Moderador]";
        else if (tag.equalsIgnoreCase("suporte")) return ChatColor.YELLOW + "[Suporte]";
        else if (tag.equalsIgnoreCase("youtuber")) return ChatColor.GOLD + "[YouTuber]";
        else if (tag.equalsIgnoreCase("vip")) return ChatColor.GOLD + "[VIP]";
        else if (tag.equalsIgnoreCase("membro")) return ChatColor.GRAY + "Membro";
        return null;
    }

    public static String sequency(String tag) {
        if (tag.equalsIgnoreCase("superior")) return "00";
        else if (tag.equalsIgnoreCase("gestor")) return "01";
        else if (tag.equalsIgnoreCase("moderador")) return "02";
        else if (tag.equalsIgnoreCase("suporte")) return "03";
        else if (tag.equalsIgnoreCase("youtuber")) return "04";
        else if (tag.equalsIgnoreCase("vip")) return "05";
        else if (tag.equalsIgnoreCase("membro")) return "06";
        return null;
    }

    public static ChatColor colorType(String tag) {
        if (tag.equalsIgnoreCase("superior")) return ChatColor.RED;
        else if (tag.equalsIgnoreCase("gestor")) return ChatColor.RED;
        else if (tag.equalsIgnoreCase("moderador")) return ChatColor.RED;
        else if (tag.equalsIgnoreCase("suporte")) return ChatColor.RED;
        else if (tag.equalsIgnoreCase("youtuber")) return ChatColor.RED;
        else if (tag.equalsIgnoreCase("vip")) return ChatColor.GOLD;
        else if (tag.equalsIgnoreCase("membro")) return ChatColor.GRAY;
        return null;
    }

    public static int space = 1;

    public static String sbSpace() {
        if (space > 9) space = 1;
        space++;
        return "§" + space;
    }
}
