package com.mcm.scoreboard;

import com.mcm.scoreboard.listeners.PlayerJoinEvents;
import com.mcm.scoreboard.listeners.PlayerLeftEvents;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerLeftEvents(), Main.plugin);
    }

    @Override
    public void onDisable() {
        //-
    }
}
