package com.mcm.scoreboard.builders;

import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.OnlineCount;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.enums.ServersList;
import com.mcm.scoreboard.utils.ScoreboardBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import java.text.DecimalFormat;
import java.util.Arrays;

public class MainScoreboard {

    public static void build(final Player player) {
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(player.getUniqueId().toString());
        final DecimalFormat formatter = new DecimalFormat("#,###.00");

        org.bukkit.scoreboard.Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Objective objective = scoreboard.registerNewObjective("McMedieval", "stats");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§6§lMinecraft Medieval");

        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(6);
        objective.getScore(Main.getTradution("MD8HjA&cfD2FuT&", uuid)).setScore(5);
        objective.getScore(Main.getTradution("jRjY$y3nA8jwn*R", uuid)).setScore(4);
        objective.getScore(Main.getTradution("4#rtTa!YDv&%YuB", uuid)).setScore(3);
        objective.getScore(Main.getTradution("Z2q!J9nWC%4WvJx", uuid)).setScore(2);
        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(1);
        objective.getScore(ChatColor.DARK_PURPLE + " www.minecraftmedieval.com").setScore(0);

        if (scoreboard.getTeam("grupo") == null) scoreboard.registerNewTeam("grupo").addEntry(Main.getTradution("MD8HjA&cfD2FuT&", uuid));
        if (scoreboard.getTeam("moeda") == null) scoreboard.registerNewTeam("moeda").addEntry(Main.getTradution("jRjY$y3nA8jwn*R", uuid));
        if (scoreboard.getTeam("guilda") == null) scoreboard.registerNewTeam("guilda").addEntry(Main.getTradution("4#rtTa!YDv&%YuB", uuid));
        if (scoreboard.getTeam("online") == null) scoreboard.registerNewTeam("online").addEntry(Main.getTradution("Z2q!J9nWC%4WvJx", uuid));

        scoreboard.getTeam("grupo").setSuffix(ScoreboardBuilder.prefix(tag));
        if (com.mcm.core.database.CoinsDb.getCoins(player.getUniqueId().toString()) != 0) {
            scoreboard.getTeam("moeda").setSuffix(ChatColor.YELLOW + "$" + formatter.format(com.mcm.core.database.CoinsDb.getCoins(player.getUniqueId().toString())));
        } else scoreboard.getTeam("moeda").setSuffix(ChatColor.YELLOW + "$0.0");
        if (GuildaDb.getGuildaP(uuid) != null) scoreboard.getTeam("guilda").setSuffix(ChatColor.AQUA + GuildaDb.getGuildaP(uuid));
        else scoreboard.getTeam("guilda").setSuffix(Main.getTradution("A%@gw8%UHM!$ccY", uuid));
        scoreboard.getTeam("online").setSuffix(ChatColor.GREEN + String.valueOf(OnlineCount.getAllOnline()));

        for (Player target : Bukkit.getOnlinePlayers()) {
            String targetTag = com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString());
            if (scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)) == null)
                scoreboard.registerNewTeam("000" + ScoreboardBuilder.sequency(targetTag)).setPrefix(ScoreboardBuilder.tabPrefix(targetTag));
            scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)).addEntry(target.getName());
            scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)).setColor(ScoreboardBuilder.colorType(targetTag));
        }
        String[] tags = {"superior", "gestor", "moderador", "suporte"};
        if (Arrays.asList(tags).contains(tag)) {
            for (ServersList server : ServersList.values()) {
                if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                    RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/staff_entry=" + tag + "=" + player.getName());
            }
        }

        player.setScoreboard(scoreboard);
        player.setPlayerListHeaderFooter(ChatColor.translateAlternateColorCodes('&', " \n&6&lMinecraft Medieval\n "), com.mcm.core.utils.CentralizeMsg.sendCenteredMessage("\n                         " +
                "&6Twitter: &fwww.twitter.com/minemedieval                          " +
                "\n&6Discord: &fwww.bit.ly/McMedievalDiscord" +
                "\n &6" + Main.getTradution("XP*dVd#BcC!z7kJ", uuid) + ": &fwww.minecraftmedieval.com\n "));
    }
}
