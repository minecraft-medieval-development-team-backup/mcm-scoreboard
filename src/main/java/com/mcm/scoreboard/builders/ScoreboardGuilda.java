package com.mcm.scoreboard.builders;

import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.OnlineCount;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.enums.ServersList;
import com.mcm.scoreboard.utils.ScoreboardBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import java.text.DecimalFormat;
import java.util.Arrays;

public class ScoreboardGuilda {

    public static void build(Player player) {
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(player.getUniqueId().toString());
        final DecimalFormat formatter = new DecimalFormat("#,###.00");
        final DecimalFormat formatterGuilda = new DecimalFormat("#.###");

        org.bukkit.scoreboard.Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Objective objective = scoreboard.registerNewObjective("McMedieval", "stats");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        objective.setDisplayName("§6§lMinecraft Medieval");

        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(12);
        objective.getScore(Main.getTradution("MD8HjA&cfD2FuT&", uuid)).setScore(11);
        objective.getScore(Main.getTradution("jRjY$y3nA8jwn*R", uuid)).setScore(10);
        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(9);
        objective.getScore(Main.getTradution("4#rtTa!YDv&%YuB", uuid)).setScore(8);
        objective.getScore(Main.getTradution("Q2@gdyBDK6yyb7q", uuid)).setScore(7);
        objective.getScore(Main.getTradution("68dC#2c@3X%Zk$7", uuid)).setScore(6);
        objective.getScore(Main.getTradution("EA&6x?Dk3u&#D*D", uuid)).setScore(5);
        objective.getScore(Main.getTradution("3kGShNptH39dQz$", uuid)).setScore(4);
        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(3);
        objective.getScore(Main.getTradution("Z2q!J9nWC%4WvJx", uuid)).setScore(2);
        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(1);
        objective.getScore(ChatColor.DARK_PURPLE + " www.minecraftmedieval.com").setScore(0);

        scoreboard.registerNewTeam("grupo").addEntry(Main.getTradution("MD8HjA&cfD2FuT&", uuid));
        scoreboard.registerNewTeam("moeda").addEntry(Main.getTradution("jRjY$y3nA8jwn*R", uuid));
        scoreboard.registerNewTeam("guilda").addEntry(Main.getTradution("4#rtTa!YDv&%YuB", uuid));
        scoreboard.registerNewTeam("xp").addEntry(Main.getTradution("Q2@gdyBDK6yyb7q", uuid));
        scoreboard.registerNewTeam("coins").addEntry(Main.getTradution("68dC#2c@3X%Zk$7", uuid));
        scoreboard.registerNewTeam("elixir").addEntry(Main.getTradution("EA&6x?Dk3u&#D*D", uuid));
        scoreboard.registerNewTeam("elixir_negro").addEntry(Main.getTradution("3kGShNptH39dQz$", uuid));
        scoreboard.registerNewTeam("online").addEntry(Main.getTradution("Z2q!J9nWC%4WvJx", uuid));

        scoreboard.getTeam("grupo").setSuffix(ScoreboardBuilder.prefix(tag));
        if (com.mcm.core.database.CoinsDb.getCoins(player.getUniqueId().toString()) != 0) {
            scoreboard.getTeam("moeda").setSuffix("$" + formatter.format(com.mcm.core.database.CoinsDb.getCoins(player.getUniqueId().toString())));
        } else scoreboard.getTeam("moeda").setSuffix("$0.0");
        if (GuildaDb.getGuildaP(uuid) != null) scoreboard.getTeam("guilda").setSuffix(GuildaDb.getGuildaP(uuid)); else scoreboard.getTeam("guilda").setSuffix(Main.getTradution("A%@gw8%UHM!$ccY", uuid));
        if (GuildaDb.getGuildaP(uuid) != null) scoreboard.getTeam("xp").setSuffix(GuildaDb.getXp(GuildaDb.getGuildaP(uuid)) + "  " + ChatColor.GOLD + "(Level " + GuildaDb.getLevel(GuildaDb.getGuildaP(uuid)) + ")"); else scoreboard.getTeam("xp").setSuffix("0  " + ChatColor.GOLD + "Level 0)");
        if (GuildaDb.getGuildaP(uuid) != null) scoreboard.getTeam("coins").setSuffix(formatterGuilda.format(GuildaDb.getCoins(GuildaDb.getGuildaP(uuid)))); else scoreboard.getTeam("coins").setSuffix("0");
        if (GuildaDb.getGuildaP(uuid) != null) scoreboard.getTeam("elixir").setSuffix(formatterGuilda.format(GuildaDb.getElixir(GuildaDb.getGuildaP(uuid)))); else scoreboard.getTeam("elixir").setSuffix("0");
        if (GuildaDb.getGuildaP(uuid) != null) scoreboard.getTeam("elixir_negro").setSuffix(formatterGuilda.format(GuildaDb.getelixir_negro(GuildaDb.getGuildaP(uuid)))); else scoreboard.getTeam("elixir_negro").setSuffix("0");
        scoreboard.getTeam("online").setSuffix(String.valueOf(OnlineCount.getAllOnline()));

        for (Player target : Bukkit.getOnlinePlayers()) {
            String targetTag = com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString());
            if (scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)) == null)
                scoreboard.registerNewTeam("000" + ScoreboardBuilder.sequency(targetTag)).setPrefix(ScoreboardBuilder.tabPrefix(targetTag));
            scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)).addEntry(target.getName());
            scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)).setColor(ScoreboardBuilder.colorType(targetTag));
        }
        String[] tags = {"superior", "gestor", "moderador", "suporte"};
        if (Arrays.asList(tags).contains(tag)) {
            for (ServersList server : ServersList.values()) {
                if (!server.name().replaceAll("_", "-").equals(Main.server_name)) RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/staff_entry=" + tag + "=" + player.getName());
            }
        }

        player.setScoreboard(scoreboard);
        player.setPlayerListHeaderFooter(ChatColor.translateAlternateColorCodes('&', " \n&6&lMinecraft Medieval\n "), com.mcm.core.utils.CentralizeMsg.sendCenteredMessage("\n                         " +
                "&6Twitter: &fwww.twitter.com/mcmedieval                          " +
                "\n&6Discord: &fbit.ly/McMedievalDiscord" +
                "\n &6" + Main.getTradution("XP*dVd#BcC!z7kJ", uuid) + ": &fwww.minecraftmedieval.com\n "));
    }
}
