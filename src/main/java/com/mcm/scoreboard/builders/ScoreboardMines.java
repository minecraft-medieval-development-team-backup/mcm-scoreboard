package com.mcm.scoreboard.builders;

import com.mcm.core.Main;
import com.mcm.core.cache.OnlineCount;
import com.mcm.core.database.MinesDb;
import com.mcm.core.utils.TimeUtil;
import com.mcm.scoreboard.utils.ScoreboardBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import java.text.DecimalFormat;

public class ScoreboardMines {

    public static void build(final Player player, int mine) {
        String uuid = player.getUniqueId().toString();
        final DecimalFormat formatter = new DecimalFormat("#,###.00");

        org.bukkit.scoreboard.Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Objective objective = scoreboard.registerNewObjective("Mines", "stats");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§6§lMinecraft Medieval");

        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(9);
        objective.getScore(Main.getTradution("jRjY$y3nA8jwn*R", uuid)).setScore(8);
        objective.getScore(Main.getTradution("Z2q!J9nWC%4WvJx", uuid)).setScore(7);
        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(6);
        objective.getScore(Main.getTradution("hs3*ew8gRYF6VDj", uuid)).setScore(5);
        objective.getScore(Main.getTradution("@EjG%CxvqJCtNu9", uuid)).setScore(4);
        objective.getScore(Main.getTradution("*p9K*F7tUY2xCup", uuid)).setScore(3);
        objective.getScore(Main.getTradution("qa!CaTc6JWxnPep", uuid)).setScore(2);
        objective.getScore(ScoreboardBuilder.sbSpace()).setScore(1);
        objective.getScore(ChatColor.DARK_PURPLE + " www.minecraftmedieval.com").setScore(0);

        if (scoreboard.getTeam("moeda") == null) scoreboard.registerNewTeam("moeda").addEntry(Main.getTradution("jRjY$y3nA8jwn*R", uuid));
        if (scoreboard.getTeam("online") == null) scoreboard.registerNewTeam("online").addEntry(Main.getTradution("Z2q!J9nWC%4WvJx", uuid));
        if (scoreboard.getTeam("grupo") == null) scoreboard.registerNewTeam("mina").addEntry(Main.getTradution("hs3*ew8gRYF6VDj", uuid));
        if (scoreboard.getTeam("tempo") == null) scoreboard.registerNewTeam("tempo").addEntry(Main.getTradution("@EjG%CxvqJCtNu9", uuid));
        if (scoreboard.getTeam("atual") == null) scoreboard.registerNewTeam("atual").addEntry(Main.getTradution("*p9K*F7tUY2xCup", uuid));
        if (scoreboard.getTeam("proximo") == null) scoreboard.registerNewTeam("proximo").addEntry(Main.getTradution("qa!CaTc6JWxnPep", uuid));

        scoreboard.getTeam("online").setSuffix(ChatColor.GREEN + String.valueOf(OnlineCount.getAllOnline()));
        if (com.mcm.core.database.CoinsDb.getCoins(player.getUniqueId().toString()) != 0) {
            scoreboard.getTeam("moeda").setSuffix(ChatColor.YELLOW + "$" + formatter.format(com.mcm.core.database.CoinsDb.getCoins(player.getUniqueId().toString())));
        } else scoreboard.getTeam("moeda").setSuffix(ChatColor.YELLOW + "$0.0");
        scoreboard.getTeam("mina").setSuffix(ChatColor.GREEN + MinesDb.getName(mine));
        scoreboard.getTeam("tempo").setSuffix(ChatColor.GREEN + TimeUtil.getTime(MinesDb.getTime(uuid, mine)));
        scoreboard.getTeam("atual").setSuffix(ChatColor.GRAY + String.valueOf(MinesDb.getXP(uuid, mine)) + ChatColor.GREEN + " (Lv" + MinesDb.getLevel(uuid, mine) + ")");
        scoreboard.getTeam("proximo").setSuffix("FAZER");

        for (Player target : Bukkit.getOnlinePlayers()) {
            String targetTag = com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString());
            if (scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)) == null)
                scoreboard.registerNewTeam("000" + ScoreboardBuilder.sequency(targetTag)).setPrefix(ScoreboardBuilder.tabPrefix(targetTag));
            scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)).addEntry(target.getName());
            scoreboard.getTeam("000" + ScoreboardBuilder.sequency(targetTag)).setColor(ScoreboardBuilder.colorType(targetTag));
        }

        player.setScoreboard(scoreboard);
        player.setPlayerListHeaderFooter(ChatColor.translateAlternateColorCodes('&', " \n&6&lMinecraft Medieval\n "), com.mcm.core.utils.CentralizeMsg.sendCenteredMessage("\n                         " +
                "&6Twitter: &fwww.twitter.com/minemedieval                          " +
                "\n&6Discord: &fwww.bit.ly/McMedievalDiscord" +
                "\n &6" + Main.getTradution("XP*dVd#BcC!z7kJ", uuid) + ": &fwww.minecraftmedieval.com\n "));
    }
}
