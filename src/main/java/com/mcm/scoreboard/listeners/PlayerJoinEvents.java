package com.mcm.scoreboard.listeners;

import com.mcm.scoreboard.utils.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        ScoreboardManager.createScoreboard(player);

        for (Player target : Bukkit.getOnlinePlayers()) {
            if (!target.getName().equals(player.getName())) {
                ScoreboardManager.updateOnline(target);
                ScoreboardManager.updateAllTab();
            }
        }
    }
}
