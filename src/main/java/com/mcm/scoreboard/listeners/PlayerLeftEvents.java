package com.mcm.scoreboard.listeners;

import com.mcm.scoreboard.Main;
import com.mcm.scoreboard.utils.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeftEvents implements Listener {

    @EventHandler
    public void onLeft(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
            for (Player target : Bukkit.getOnlinePlayers()) {
                if (!target.getName().equals(player.getName())) {
                    ScoreboardManager.updateOnline(target);
                    ScoreboardManager.updateAllTab();
                }
            }
        }, 20L);
    }
}
